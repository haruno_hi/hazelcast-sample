package com.haru.hazelcast.config;

import com.haru.hazelcast.repository.HUserCallRepository;
import com.hazelcast.config.ClasspathXmlConfig;
import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.data.hazelcast.HazelcastKeyValueAdapter;
import org.springframework.data.hazelcast.repository.HazelcastRepository;
import org.springframework.data.hazelcast.repository.config.EnableHazelcastRepositories;
import org.springframework.data.keyvalue.core.KeyValueOperations;
import org.springframework.data.keyvalue.core.KeyValueTemplate;
import org.springframework.data.keyvalue.core.mapping.context.KeyValueMappingContext;
//import org.springframework.data.redis.core.RedisKeyValueAdapter;
//import org.springframework.data.redis.core.RedisKeyValueTemplate;
//import org.springframework.data.redis.core.convert.*;
//import org.springframework.data.redis.core.index.IndexConfiguration;
//import org.springframework.data.redis.core.mapping.RedisMappingContext;

@Configuration
//@EnableHazelcastRepositories(basePackageClasses = HazelcastRepository.class, keyValueTemplateRef="hazelcastKeyValueTemplate")
//@EnableHazelcastRepositories(basePackageClasses = HUserCallRepository.class)
@EnableHazelcastRepositories(basePackageClasses = HUserCallRepository.class)
//@EnableHazelcastRepositories(basePackages = {"com.haru.hazelcast.repository.*"})
public class HazelcastConfig {


    @Bean
    HazelcastInstance hazelcastInstance() {
//        Config config = new Config();
        Config config = new ClasspathXmlConfig("hazelcast.xml");
        config.setInstanceName("hazelcast-instance");
//        ManagementCenterConfig manCenterCfg = new ManagementCenterConfig();
//        manCenterCfg.setEnabled(true).setUrl("http://localhost:8080/mancenter");
//        config.setInstanceName()

//        JoinConfig joinConfig = config.getNetworkConfig().getJoin();
//        joinConfig.getMulticastConfig().setEnabled(false);
//        TcpIpConfig tcpIpConfig = joinConfig.getTcpIpConfig();
////        ClientConfig clientConfig = new XmlClientConfigBuilder("hazelcast-client.xml").build();
//
//
//        config.getNetworkConfig().setPort(5701);
//        config.getNetworkConfig().setPortAutoIncrement(false);

        return Hazelcast.newHazelcastInstance(config);
        // return HazelcastClient.newHazelcastClient();
    }


/*

    @Bean
    public RedisKeyValueTemplate redisKeyValueTemplate(RedisKeyValueAdapter redisKeyValueAdapter, RedisMappingContext redisMappingContext) {
        return new RedisKeyValueTemplate(redisKeyValueAdapter,redisMappingContext);
    }


    @Bean
    public MappingRedisConverter redisConverter(RedisCustomConversions customConversions, ReferenceResolver referenceResolver) {

        MappingRedisConverter mappingRedisConverter = new MappingRedisConverter(redisKeyValueMappingContext(), null, referenceResolver,
                customTypeMapper());

        mappingRedisConverter.setCustomConversions(customConversions);

        return mappingRedisConverter;
    }

    @Bean
    public RedisTypeMapper customTypeMapper() {
        return new DefaultRedisTypeMapper();
    }


    @Bean
    public RedisMappingContext redisKeyValueMappingContext() {
        return new RedisMappingContext(new MappingConfiguration(new IndexConfiguration(), new KeyspaceConfiguration()));
    }

    */
}
