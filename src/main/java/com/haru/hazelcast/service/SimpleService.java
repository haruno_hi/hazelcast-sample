package com.haru.hazelcast.service;

import com.haru.hazelcast.domain.HUserCall;
import com.haru.hazelcast.repository.HUserCallRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Slf4j
@Service
public class SimpleService {

    @Autowired
    private HUserCallRepository hUserCallRepository;


    public HUserCall add(HUserCall hUserCall) {
        return hUserCallRepository.save(hUserCall);
    }

    public List<HUserCall> findByMdnIn(List<String> mdns) {
        return hUserCallRepository.findByMdnIn(mdns);
    }

    public List<HUserCall> findByMdn(String mdn) {
        return hUserCallRepository.findByMdn(mdn);
    }

    public List<HUserCall> findAll() {
        return StreamSupport.stream(hUserCallRepository.findAll().spliterator(), false).collect(Collectors.toList());
    }

    public void delete(HUserCall hUserCall) {
        hUserCallRepository.delete(hUserCall);
    }


}
