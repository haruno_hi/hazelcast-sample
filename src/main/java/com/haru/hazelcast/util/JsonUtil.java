package com.haru.hazelcast.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class JsonUtil {
	static ObjectMapper mapper = new ObjectMapper();
	static ObjectMapper prettyMapper = new ObjectMapper();
	static {
        prettyMapper.enable(SerializationFeature.INDENT_OUTPUT);
        prettyMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	public static <T> T toObject(String json, Class<T> clazz) throws Exception {
		return mapper.readValue(json, clazz);
	}


	public static String toJson(Object obj) {
		try {
			return mapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			throw new JsonEncodeException(e);
		}
	}

	public static String toPrettyJson(Object obj) {
		try {
			return prettyMapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			throw new JsonEncodeException(e);
		}
	}

	public static <T> T toObject(JsonNode json, Class<T> clazz) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.treeToValue(json, clazz);
		} catch (JsonProcessingException e) {
			throw new JsonDecodeException(e);
		}
	}

	public static JsonNode toJsonNode(Object obj) {
		return mapper.convertValue(obj, JsonNode.class);
	}

	public static class JsonEncodeException extends RuntimeException {
		public JsonEncodeException(Throwable cause) {
			super(cause);
		}
	}

	public static class JsonDecodeException extends RuntimeException {
		public JsonDecodeException(Throwable cause) {
			super(cause);
		}
	}
}
