package com.haru.hazelcast.controller;


import com.haru.hazelcast.domain.HUserCall;
import com.haru.hazelcast.service.SimpleService;
import com.haru.hazelcast.util.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/hazel")
public class SimpleController {

    @Autowired
    private SimpleService simpleService;

    @GetMapping("/findBy/{mdn}")
    @ResponseBody
    public String findBy(@PathVariable String mdn){
        List<HUserCall> userCalls = simpleService.findByMdn(mdn);
        return JsonUtil.toJson(userCalls);
    }

    @GetMapping("/findAll")
    @ResponseBody
    public String findAll(){
        List<HUserCall> userCalls = simpleService.findAll();
        return JsonUtil.toJson(userCalls);
    }


    @PutMapping("/add/{mdn}")
    @ResponseBody
    public String add(@PathVariable String mdn){
        HUserCall newCall = HUserCall.builder().mdn(mdn).build();
        HUserCall userCall = simpleService.add(newCall);
        return JsonUtil.toJson(userCall);
    }

    @DeleteMapping("/del/{mdn}")
    @ResponseBody
    public String del(@PathVariable String mdn){
        HUserCall newCall = HUserCall.builder().mdn(mdn).build();
        simpleService.delete(newCall);
        return JsonUtil.toJson("");
    }
}
