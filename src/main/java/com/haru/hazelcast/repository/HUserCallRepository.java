package com.haru.hazelcast.repository;

import com.haru.hazelcast.domain.HUserCall;
import org.springframework.data.hazelcast.repository.HazelcastRepository;
import org.springframework.data.keyvalue.repository.KeyValueRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HUserCallRepository extends HazelcastRepository<HUserCall, String> {

    List<HUserCall> findByMdn(String mdn);

    List<HUserCall> findByMdnIn(List<String> mdns);


}