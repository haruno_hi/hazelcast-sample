package com.haru.hazelcast.domain;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.keyvalue.annotation.KeySpace;

import java.io.Serializable;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@KeySpace("ACTIVATED_USER_CALL")
public class HUserCall implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private String mdn;

	private boolean activated;
}
